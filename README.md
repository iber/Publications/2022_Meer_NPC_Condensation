# **2022_Meer_NPC_Condensation**


A. Sharma, M. Meer, A. Dapkunas, A. Ihermann-Hella, S. Kuure, S. Vainio, D. Iber, F. Naillat, 
"FGF8 induces chemokinesis and regulates condensation of mouse nephron progenitor cells". 
_Development (2022) 149 (21): dev201012._ [https://doi.org/10.1242/dev.201012](https://doi.org/10.1242/dev.201012)




# Supplementary Material

- Videos 1-3

- ZIP file containing the source code for the NPC condensation simulations. For more information on the C++ simulation framework, cf. the [LBIBCell documentation](https://tanakas.bitbucket.io/lbibcell/index.html) and the [corresponding publication](https://doi.org/10.1093/bioinformatics/btv147).
